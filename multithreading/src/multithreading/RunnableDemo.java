// Practice code with Runnable.  This creates two threads and prints out the status for each step in the process
// The code does nothing useful, it is simply good practice

package multithreading;

// Implement Runnable which allows instances of this class to be its own thread
public class RunnableDemo implements Runnable {
	private Thread t;
	private String threadName;
	
	RunnableDemo(String name) {
		threadName = name;
		System.out.println("Creating thread: " + threadName );
	}

	public void run() {
		System.out.println("Running thread " + threadName );
		try {
			for (int i = 4; i > 0; i--) {
				System.out.println("Thread: " + threadName + ", " + i );
				Thread.sleep(50);
			}
		} catch (InterruptedException e) {
			System.out.println("Interrupted thread: " + threadName );
		}
		
		System.out.println("Exiting thread: " + threadName );
	}
	
	public void start() {
		System.out.println("Starting thread " + threadName );
		if (t == null) {
			t = new Thread(this, threadName);
			t.start();
		}
	}
	
	public static void main(String[] args) {
		RunnableDemo thread1 = new RunnableDemo("first");
		RunnableDemo thread2 = new RunnableDemo("second");
		
		thread1.start();
		thread2.start();
		
	}
}
